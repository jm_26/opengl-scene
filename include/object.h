#pragma once 

#include "glm/gtc/matrix_transform.hpp"

#include "vertices.h"
#include "vertex_buffer.h"
#include "vertex_array_object.h"
#include "shader.h"
#include "material.h"
#include "camera.h"

class Object
{
public: 

    Object(const Material& material, glm::vec3 scale, glm::vec3 position);

    ~Object();

    void draw();

    void draw(const glm::vec3& scale, const glm::vec3& position);

    inline void setMaterial(const Material& mat) { material = mat; }

private: 

    Shader* shader;

    VertexBuffer* VBO;

    VertexArrayObject* VAO;

    glm::vec3 scale;
    glm::vec3 position;

    glm::mat4 model;

    Material material;
};