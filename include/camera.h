/* 

Camera class simulating 3D "flying" camera view

*/


#pragma once 

#include "glm/gtc/matrix_transform.hpp"


enum class CameraMovement
{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

class Camera
{
public: 
    
    glm::vec3 Position;

    glm::vec3 Front   = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 Up      = glm::vec3(0.0f, 1.0f,  0.0f);
    glm::vec3 Right   = glm::vec3(1.0f, 0.0f,  0.0f);

    const glm::vec3 WorldUp = glm::vec3(0.0f, 1.0f, 0.0f);

    float Yaw   = -90.0f;
    float Pitch = 0.0f;
    float Zoom  = 45.0f;

    const float MouseSensitivity = 0.1f;
    const float Speed            = 2.5f;

    Camera(const glm::vec3& position) : Position(position)
    {
        updateCameraVectors();
    }
    
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(Position, Position + Front, Up);
    }

    void UpdateCameraPosition(CameraMovement direction, float deltaTime)
    {
        const float velocity = Speed * deltaTime;

        switch (direction)
        {
            case CameraMovement::FORWARD:
                Position += Front * velocity;
                break;
            case CameraMovement::BACKWARD: 
                Position -= Front * velocity;
                break;
            case CameraMovement::RIGHT: 
                Position += Right * velocity;
                break;
            case CameraMovement::LEFT:
                Position -= Right * velocity;
                break;
        }
    }

    void ProcessMouseMovement(float xoffset, float yoffset)
    {
        xoffset *= MouseSensitivity;
        yoffset *= MouseSensitivity;

        Yaw   += xoffset;
        Pitch -= yoffset;

        if (Pitch >  89.0f) Pitch =  89.0f;
        if (Pitch < -89.0f) Pitch = -89.0f;

        updateCameraVectors();
    }

    void ProcessMouseZoom(float yoffset)
    {
        Zoom -= yoffset;

        if (Zoom < 1.0f)  Zoom = 1.0f;
        if (Zoom > 45.0f) Zoom = 45.0f;
    }

private: 

    void updateCameraVectors()
    {
        // Spherical coordinates

        glm::vec3 front; 
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));

        Front = glm::normalize(front);
        Right = glm::normalize(glm::cross(Front, WorldUp));
        Up    = glm::normalize(glm::cross(Right, Front));
    }
};

