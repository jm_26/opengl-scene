#pragma once 

#include "glad.h"

#include <vector>

class VertexBuffer
{

public: 

    VertexBuffer() {}

    VertexBuffer(const std::vector<float>& vertices)
    {
        glGenBuffers(1, &ID);
        glBindBuffer(GL_ARRAY_BUFFER, ID);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    inline void Bind() const 
    {
        glBindBuffer(GL_ARRAY_BUFFER, ID);
    }

    inline void Unbind() const 
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

private: 

    unsigned int ID;
};
