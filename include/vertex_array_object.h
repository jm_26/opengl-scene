#pragma once 

#include "glad.h"

#include <vector> 

enum class VAOLayout
{
    POSITIONS,
    POSITIONS_NORMALS
};

class VertexArrayObject
{

public: 

    VertexArrayObject() {}

    VertexArrayObject(const VAOLayout& layout)
    {
        unsigned int vertex_attrib_count = ChooseLayout(layout);

        glGenVertexArrays(1, &ID);
        glBindVertexArray(ID);

        unsigned int pointer = 0;
        
        for (unsigned int i = 0; i < vertex_attrib_count; ++i)
        {
            glVertexAttribPointer(i, 
                                  attrib_layout[i], 
                                  GL_FLOAT,
                                  GL_FALSE,
                                  stride * sizeof(float), 
                                  (const void*)(pointer * sizeof(float)));

           pointer += attrib_layout[i];

           glEnableVertexAttribArray(i);

        }

        glBindVertexArray(0);
    }

    inline void Bind() const 
    {
        glBindVertexArray(ID);
    }

    inline void Unbind() const 
    {
        glBindVertexArray(0);
    }

private: 

    unsigned int ID;

    std::vector<unsigned int> attrib_layout; 

    unsigned int stride;

    int ChooseLayout(const VAOLayout& layout)
    {
        switch(layout)
        {
            case VAOLayout::POSITIONS: 
                attrib_layout = {3};
                stride = 3;
                return 1;
                break;
            case VAOLayout::POSITIONS_NORMALS: 
                attrib_layout = {3, 3};
                stride = 3 + 3;
                return 2;
                break;
            default:
                return 0;
                break;
        }
    }
};
