#pragma once 

#include "glm/gtc/matrix_transform.hpp"

struct Material
{
    float shininess;

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;

    Material() {}

    Material(float shininess, glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular)
        : shininess(shininess), ambient(ambient), diffuse(diffuse), specular(specular) {}
};

struct MaterialFactory
{
    static Material GetGoldMaterial()
    {
        return Material { 
                          0.4f * 128, 
                          glm::vec3(0.24725f, 0.1995f, 0.0745f), 
                          glm::vec3(0.75164f, 0.60648f, 0.22648f), 
                          glm::vec3(0.628281f, 0.555802f, 0.366065f)
                        }; 
    }

    static Material GetObsidianMaterial()
    {
        return Material {
                          38.4f,
                          glm::vec3{0.05375f, 0.05f, 0.0662f}, 
                          glm::vec3(0.18275f, 0.17f, 0.22525f),
                          glm::vec3(0.332741f, 0.328634f, 0.346435f)
                        };
    }

    static Material GetDarkWoodMaterial()
    {
        return Material {
                          10.0f,
                          glm::vec3{0.2f, 0.1f, 0.0f}, 
                          glm::vec3(0.4f, 0.1f, 0.0f),
                          glm::vec3(0.1f, 0.1f, 0.1f)
                        };
    }

    static Material GetMarbleMaterial()
    {
        return Material {
                          50.0f,
                          glm::vec3{0.9f, 0.9f, 0.9f}, 
                          glm::vec3(1.0f, 1.0f, 1.0f),
                          glm::vec3(0.5f, 0.5f, 0.5f)
                        };
    }

    static Material GetCreamMaterial()
    {
        return Material {
                          20.0f,
                          glm::vec3{0.99f, 0.99f, 0.95f}, 
                          glm::vec3(1.0f, 1.0f, 0.95f),
                          glm::vec3(0.5f, 0.5f, 0.5f)
                        };
    }

    static Material GetLightWoodMaterial()
    {
        return Material {
                          20.0f,
                          glm::vec3{0.4f, 0.25f, 0.1f}, 
                          glm::vec3(0.8f, 0.6f, 0.3f),
                          glm::vec3(0.05f, 0.05f, 0.05f)
                        };
    }

    static Material GetPlantMaterial()
    {
        return Material {
                          76.0f,
                          glm::vec3{0.0215f, 0.1745f, 0.0215f}, 
                          glm::vec3(0.07568f, 0.61424f, 0.07568f),
                          glm::vec3(0.633f, 0.727811f, 0.633f)
                        };
    }

    static Material GetLeatherMaterial()
    {
        return Material {
                          76.0f,
                          glm::vec3{0.25f, 0.148f, 0.06475f}, 
                          glm::vec3(0.4f, 0.2368f, 0.1036f),
                          glm::vec3(0.7746f, 0.4586f, 0.2006f)
                        };
    }

    static Material GetCarpetMaterial()
    {
        return Material {
                          5.0f,
                          glm::vec3{0.3f, 0.15f, 0.1f}, 
                          glm::vec3(0.7f, 0.35f, 0.2f),
                          glm::vec3(0.1f, 0.1f, 0.1f)
                        };
    }

    static Material GetTVMaterial()
    {
        return Material {
                          100.0f,
                          glm::vec3{0.05f, 0.05f, 0.05f}, 
                          glm::vec3(0.1f, 0.1f, 0.1f),
                          glm::vec3(0.7f, 0.7f, 0.7f)
                        };
    }

    static Material GetWallMaterial()
    {
        return Material {
                          1.0f,
                          glm::vec3{0.8f, 0.8f, 0.8f}, 
                          glm::vec3(1.0f, 1.0f, 1.0f),
                          glm::vec3(0.1f, 0.1f, 0.1f)
                        };
    }

    static Material GetVeryDarkWoodMaterial()
    {
        return Material {
                          10.0f,
                          glm::vec3{0.1f, 0.05f, 0.025f}, 
                          glm::vec3(0.2f, 0.1f, 0.05f),
                          glm::vec3(0.05f, 0.05f, 0.05f)
                        };
    }

    static Material GetDarkBrownWoodMaterial()
    {
        return Material {
                          10.0f,
                          glm::vec3{0.2f, 0.1f, 0.05f}, 
                          glm::vec3(0.4f, 0.2f, 0.1f),
                          glm::vec3(0.05f, 0.05f, 0.05f)
                        };
    }

    static Material GetPillowMaterial()
    {
        return Material {
                          10.0f,
                          glm::vec3{0.0f, 0.0f, 0.4f}, 
                          glm::vec3(0.0f, 0.0f, 0.8f),
                          glm::vec3(0.3f, 0.3f, 0.3f)
                        };
    }

    static Material GetFloorMaterial()
    {
        return Material {
                          20.0f,
                          glm::vec3{0.6f, 0.6f, 0.5f}, 
                          glm::vec3(0.9f, 0.9f, 0.8f),
                          glm::vec3(0.3f, 0.3f, 0.3f)
                        };
    }
};

