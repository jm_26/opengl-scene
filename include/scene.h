#pragma once 

#include "glm/gtc/matrix_transform.hpp"

#include "object.h"

#include <vector> 

class Scene 
{
public: 

    Scene();

    ~Scene();

    void display();

private: 

    std::vector<Object*> objects;

    const unsigned int panels_count = 5;
    const unsigned int floor_planks_count = 11;
};