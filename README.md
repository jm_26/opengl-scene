# 3D Scene Diplay

# Table of Contents
- [Project Description](#project-description)
- [Installation](#installation)
- [Usage](#usage)
- [Project Details](#project-details)
- [Author](#author)
- [References](#references)

## Project Description

This project creates a simple 3D model of a room using **OpenGL** and the **C++** programming language.

## Prerequisites

To run this program, you will need:

- Meson build system
- Python
- OpenGL 
- GLFW

Installation for Ubuntu-based distributions:

`sudo apt update`
`sudo apt install meson python3 libgl1-mesa-dev libglfw3-dev`

Installation for other distributions is analogous.

## Installation

This project uses the Meson build system. Below are the instructions to build and run the project.

1. Clone this repository:

`git clone https://gitlab.com/jm_26/opengl-scene/`

2. Navigate into the project directory:

`cd opengl-scene`

3. Configure the project:

`meson setup builddir`

4. Build the project:

`meson compile -C builddir`

5. Navigate into the *builddir* directory:

`cd builddir`

6. Execute the program:

`./prog`

## Usage

The user can interact with the program using the mouse and keyboard to move throughout the scene. The <kbd>WSAD</kbd> keys are used to move in the given direction, and the mouse is used to change the camera direction. You can also zoom in and out using the mouse wheel. A 'flying camera' is implemented, allowing vertical movement in the scene.

## Project Details

As mentioned above, this project creates a simple 3D scene using the OpenGL API. The OpenGL setup (Shaders, Camera, Lighting ...) is largely based on the [Learn OpenGL Book](https://learnopengl.com/), particularly the *Getting Started* and *Lighting* chapters. Additional features and classes have also been added. The window creation is handled using the GLFW library and for the mathematics, specifically matrix transformations, the [glm](https://github.com/g-truc/glm) header-only library is used.


**Short summary of the OpenGL setup:** 

The vertices used for the blocks in the scene are defined in the *vertices.h* header file, where each vertex consists of its coordinates and normals used for lighting. This data is loaded into the vertex buffers (*vertex_buffer.h*), and its layout is defined in the vertex array object (*vertex_array_object.h*).

Only the vertex and fragment shaders are implemented. In the vertex shader (*shaders/v_cube.glsl*), the model, view, and projection matrices are sent from the CPU side. These are processed within the shader, and fragment positions and normals are sent to the fragment shader (*shaders/f_cube.glsl*). In the fragment shader, the Phong lighting model (ambient, diffuse, and specular components of the light) is implemented. The appearance of the blocks is determined using materials (*shaders/f_cube.glsl* and *material.h*). Textures (*texture.h*) are implemented but not used in this program. 

The camera (*camera.h*) is implemented as a 'Flying' camera model, often seen in First-Person Shooter (FPS) games, but with an added degree of freedom. Please note that collisions have not been implemented, so it is possible to 'fly into the walls'. 

Lastly, a scene (*scene.h*) is added to contain all the blocks and then draw them inside the program's main loop. 

## Author
Jan Miesbauer,
FNSPE CTU, branch Mathematical Informatics,
Academic year 2022-23.

## References

**OpenGL setup**

[Learn OpenGL Book](https://learnopengl.com/).

**Matrix library**

[glm](https://github.com/g-truc/glm)

**Model inspiration**

[Model](https://www.cgtrader.com/free-3d-models/interior/living-room/simple-living-room-cc759e32-bb08-4807-8771-2d97c062bef3)

**Materials**

[Materials](http://devernay.free.fr/cours/opengl/materials.html) and collaboration with ChatGPT.
