#version 330 core

out vec4 FragColor; 

in vec3 Normal;
in vec3 FragPos;

struct Material
{
    float shininess;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct Light
{
    vec3 position; 

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform vec3 eyePos;
uniform Material material;
uniform Light light;

void main()
{
    // Phong lightning model 

    // ambient 
    float ambientStrength = 1.0f;
    vec3 ambient = ambientStrength * light.ambient * material.ambient;

    // diffuse
    vec3 normal = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diffuseStrength = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = diffuseStrength * light.diffuse * material.diffuse;

    // specular 
    vec3 viewDir = normalize(eyePos - FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float specularStrength = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = specularStrength * light.specular * material.specular;

    vec3 combined_light = ambient + diffuse + specular;
    FragColor = vec4(combined_light, 1.0);
}
