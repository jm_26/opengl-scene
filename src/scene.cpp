#include "scene.h"

using namespace glm;

Scene::Scene()
{
    // Walls 
    objects.push_back(new Object(MaterialFactory::GetObsidianMaterial(), vec3(4.0f, 0.2f, 4.0f), vec3(0.0f, -0.1, 0.0f)));
    objects.push_back(new Object(MaterialFactory::GetWallMaterial(), vec3(0.2f, 4.0f, 4.0f), vec3(-1.90f, 2.0f, 0.0f)));
    objects.push_back(new Object(MaterialFactory::GetWallMaterial(), vec3(4.0f, 4.0f, 0.2f), vec3(0.0f, 2.0f, -1.90f)));

    // Side panels
    objects.push_back(new Object(MaterialFactory::GetVeryDarkWoodMaterial(), vec3(0.1f, 3.98f, 0.4f), vec3(-1.75f, 2.01f, 1.8f)));
    objects.push_back(new Object(MaterialFactory::GetVeryDarkWoodMaterial(), vec3(0.1f, 3.98f, 0.4f), vec3(-1.75f, 2.01f, -1.6f)));

    // Middle panels
    for (unsigned int i = 0; i < panels_count; ++i)
        objects.push_back(new Object(MaterialFactory::GetVeryDarkWoodMaterial(), vec3(0.1f, 0.5f, 2.0f), vec3(-1.75f, 3.0f - 0.55f * i, 0.1f)));

    // Floor
    for(unsigned int i = 0; i < floor_planks_count; ++i)
        objects.push_back(new Object(MaterialFactory::GetFloorMaterial(), vec3(0.32f, 0.02f, 3.8f), vec3(1.84f - 0.35f * i, 0.01f, 0.1f)));

    // Couch 
    objects.push_back(new Object(MaterialFactory::GetLeatherMaterial(), vec3(1.6f, 0.598f, 0.2f), vec3(-0.0f, 0.301f, 1.6f)));
    objects.push_back(new Object(MaterialFactory::GetLeatherMaterial(), vec3(0.1f, 0.398f, 0.4f), vec3(0.75f, 0.201f, 1.3f)));
    objects.push_back(new Object(MaterialFactory::GetLeatherMaterial(), vec3(0.1f, 0.398f, 0.4f), vec3(-0.75f, 0.201f, 1.3f)));
    objects.push_back(new Object(MaterialFactory::GetLeatherMaterial(), vec3(1.4f, 0.198f, 0.4f), vec3(0.0f, 0.101f, 1.3f)));

    // Pillow 
    objects.push_back(new Object(MaterialFactory::GetPillowMaterial(), vec3(0.2f, 0.1f, 0.4f), vec3(-0.6f, 0.25f, 1.3f)));

    // Carpet
    objects.push_back(new Object(MaterialFactory::GetCarpetMaterial(), vec3(2.2f, 0.01f, 1.6f), vec3(0.0f, 0.02f, 0.0f)));

    // Table
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.7f, 0.1f, 0.5f), vec3(0.0f, 0.35f, 0.0f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.1f, 0.3f, 0.1f), vec3(-0.3f, 0.15f, -0.2f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.1f, 0.3f, 0.1f), vec3(0.3f, 0.15f, -0.2f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.1f, 0.3f, 0.1f), vec3(0.3f, 0.15f, 0.2f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.1f, 0.3f, 0.1f), vec3(-0.3f, 0.15f, 0.2f)));

    // Tv and tv controller 
    objects.push_back(new Object(MaterialFactory::GetTVMaterial(), vec3(2.0f, 1.3f, 0.1f), vec3(0.0f, 2.0f, -1.8f)));
    objects.push_back(new Object(MaterialFactory::GetObsidianMaterial(), vec3(0.06f, 0.02f, 0.2), vec3(0.2f, 0.4f, -0.1f)));

    // Drawer 
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.05f, 0.4f, 0.4f), vec3(1.2f, 0.2f, -1.6f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.05f, 0.4f, 0.4f), vec3(1.6f, 0.2f, -1.6f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.35f, 0.05, 0.4f), vec3(1.4f, 0.375, -1.6f)));
    objects.push_back(new Object(MaterialFactory::GetLightWoodMaterial(), vec3(0.35f, 0.05, 0.4f), vec3(1.4f, 0.2, -1.6f)));

    // Flower pot 
    objects.push_back(new Object(MaterialFactory::GetDarkWoodMaterial(), vec3(0.02f, 0.1f, 0.1f), vec3(1.4f, 0.45f, -1.6f)));
    objects.push_back(new Object(MaterialFactory::GetDarkWoodMaterial(), vec3(0.02f, 0.1f, 0.1f), vec3(1.5f, 0.45f, -1.6f)));
    objects.push_back(new Object(MaterialFactory::GetDarkWoodMaterial(), vec3(0.1f, 0.1f, 0.02f), vec3(1.45f, 0.45f, -1.64f)));
    objects.push_back(new Object(MaterialFactory::GetDarkWoodMaterial(), vec3(0.1f, 0.1f, 0.02f), vec3(1.45f, 0.45f, -1.56f)));

    // Soil 
    objects.push_back(new Object(MaterialFactory::GetObsidianMaterial(), vec3(0.08f, 0.1f, 0.06f), vec3(1.45f, 0.45f, -1.6f)));

    // Cactus
    objects.push_back(new Object(MaterialFactory::GetPlantMaterial(), vec3(0.05f, 0.15f, 0.05f), vec3(1.45f, 0.55f, -1.6f)));
}

Scene::~Scene()
{
    for (Object* object : objects)
        delete object;

    objects.clear();
}

void Scene::display()
{
    for (Object* object : objects)
        object->draw();
}