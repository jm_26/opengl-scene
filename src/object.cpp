#include "object.h"

extern Camera camera;

Object::Object(const Material& material, glm::vec3 scale, glm::vec3 position)
{
    // Load shaders from files
    shader = new Shader("../src/shaders/v_cube.glsl", "../src/shaders/f_cube.glsl");

    // Set and bind VBO and VAO
    VBO = new VertexBuffer(VerticesData::positions_normals);
    VBO->Bind();

    VAO = new VertexArrayObject(VAOLayout::POSITIONS_NORMALS);

    this->scale = scale;
    this->position = position;

    // Set material
    setMaterial(material);
}

Object::~Object()
{
    delete shader;
    delete VBO;
    delete VAO;
}

void Object::draw()
{
      // Use the shader
    shader->use();

    // Set uniforms
    shader->setVec3("eyePos", camera.Position);

    shader->setFloat("material.shininess", material.shininess);
    shader->setVec3("material.ambient", material.ambient);
    shader->setVec3("material.diffuse", material.diffuse);
    shader->setVec3("material.specular", material.specular);

    shader->setVec3("light.position", glm::vec3(1.0f, 3.0f, 3.0f));
    shader->setVec3("light.ambient",  glm::vec3(0.1f, 0.1f, 0.1f));
    shader->setVec3("light.diffuse",  glm::vec3(0.8f, 0.7f, 0.6f));
    shader->setVec3("light.specular", glm::vec3(0.9f, 0.9f, 0.9f));

    // Set MVP matrices
    glm::mat4 view = camera.GetViewMatrix();
    shader->setMat4("view", view);

    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), 800.0f / 600.0f, 0.1f, 100.0f);
    shader->setMat4("projection", projection);

    glm::mat4 model(1.0f);
    model = glm::translate (model, this->position);
    model = glm::scale(model, this->scale);
    shader->setMat4("model", model);

    // Issue the draw call
    VAO->Bind(); 
    glDrawArrays(GL_TRIANGLES, 0, 36);
}

void Object::draw(const glm::vec3& scale, const glm::vec3& position)
{
    // Use the shader
    shader->use();

    // Set uniforms
    shader->setVec3("eyePos", camera.Position);

    shader->setFloat("material.shininess", material.shininess);
    shader->setVec3("material.ambient", material.ambient);
    shader->setVec3("material.diffuse", material.diffuse);
    shader->setVec3("material.specular", material.specular);

    shader->setVec3("light.position", glm::vec3(4.0f, 2.0f, 2.0f));
    shader->setVec3("light.ambient",  glm::vec3(0.1f, 0.1f, 0.1f));
    shader->setVec3("light.diffuse",  glm::vec3(0.8f, 0.7f, 0.6f));
    shader->setVec3("light.specular", glm::vec3(0.9f, 0.9f, 0.9f));

    // Set MVP matrices
    glm::mat4 view = camera.GetViewMatrix();
    shader->setMat4("view", view);

    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), 800.0f / 600.0f, 0.1f, 100.0f);
    shader->setMat4("projection", projection);

    glm::mat4 model(1.0f);
    model = glm::translate (model, position);
    model = glm::scale(model, scale);
    shader->setMat4("model", model);

    // Issue the draw call
    VAO->Bind(); 
    glDrawArrays(GL_TRIANGLES, 0, 36);
}